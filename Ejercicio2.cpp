/*
 * Escuela:
 * Curso:
 * Profesor:
 * alumno:
 * Fecha:
 * Descripcion:
 * 
 * 
 */
#include <iostream>
#include <stdio.h>
#include <stdlib.h>




using namespace std;

//listacirculares


struct NodoC{
	int datoC;
	struct NodoC* siguienteC;
};

typedef struct NodoC* PListaC;


PListaC CrearNodoC(int datoC){
	PListaC p = new (struct NodoC);
	p->datoC = datoC;
	p->siguienteC = p;
	return p;
	
}
void RecorrerListaC(PListaC &listaC){
	
	PListaC p = listaC;
	if(listaC == NULL){
		cout<<"null"<<endl;
	}else{
		while(p->siguienteC!= listaC){
		cout<<p->datoC<<"->";
		p = p -> siguienteC;
	    }
	    cout<<p->datoC<<"->";
	
	    cout<<"null"<<endl;
	}
	
	
}
void InsertarPrincipioC(PListaC &listaC,int datoC){
	
	PListaC nuevo = CrearNodoC(datoC);
	if(listaC == NULL){
		listaC = nuevo;
	    nuevo->siguienteC = listaC;
	}else{
		PListaC p = listaC;
	    nuevo->siguienteC =listaC;
	    while(p->siguienteC!=listaC){
	    	p = p->siguienteC;
		}
		listaC = nuevo;
		p->siguienteC = listaC;
	}

}
//listacirculares
//listaenlazadas
struct NodoSE{
	int datoSE;
	struct NodoSE* siguienteSE;
};

typedef struct NodoSE* PListaSE;


PListaSE CrearNodoSE(int datoSE){
	PListaSE p = new (struct NodoSE);
	p->datoSE=datoSE;
	p->siguienteSE=NULL;
	return p;
	
}
void RecorrerListaSE(PListaSE &listaSE){
	
	PListaSE p = listaSE;
	while(p != NULL){
		cout<<p->datoSE<<"->";
		p = p->siguienteSE;
	
	}
	cout<<"null"<<endl;
	
}
void InsertarPrincipioSE(PListaSE &listaSE,int datoSE){
	PListaSE nuevo=CrearNodoSE(datoSE);
	if(listaSE == NULL){
		listaSE = nuevo;
	}else{
	    nuevo->siguienteSE=listaSE;
	    listaSE = nuevo;
	}

}
//listaenlazadas
struct Nodo{
	int dato;
	struct Nodo* siguiente;
	struct Nodo* anterior;
};

typedef struct Nodo* PLista;


PLista CrearNodo(int  dato){
	PLista p = new (struct Nodo);
	p->dato=dato;
	p->siguiente= NULL;
	p->anterior= NULL;
	return p;
	
}

void RecorrerLista(PLista &lista){
	
	PLista p = lista;
	while(p != NULL){
		cout<<p->dato<<"->";
		p = p->siguiente;
	
	}
	cout<<"null"<<endl;
	
}

void InsertarPrincipio(PLista &lista,int dato){
	PLista nuevo =CrearNodo(dato);
	
	if(lista == NULL){
		
		lista=nuevo;
		
	}else{
		nuevo->siguiente = lista;
	    nuevo->anterior = NULL;
		lista->anterior=nuevo;
		lista=nuevo;
	}
	
}
void InsertarFinal(PLista &lista,int dato){
	PLista nuevo=CrearNodo(dato);
	if(lista == NULL){
		lista = nuevo;
	}else{
		PLista aux=lista;
		
		while(aux->siguiente!=NULL){
			aux=aux->siguiente;	
		}
		aux->siguiente=nuevo;
		nuevo->anterior=aux;
		nuevo->siguiente=NULL;
		
	}

}
int CantidadDeElementos(PLista &lista){
	PLista p=lista;
	int cantidad=1;
	while(p->siguiente!=NULL){
		p=p->siguiente;
		cantidad=cantidad+1;
	}
	return cantidad;
}
int HallarMedia(PLista &lista){
	PLista p = lista;
	int suma=0;
	int media;
	while(p != NULL){
	    suma=suma+p->dato;
		p = p->siguiente;
		
	
	}
	media=suma/CantidadDeElementos(lista);
	return media;

}
void Distribuir(PLista &lista,PListaC &listacircular,PListaSE &listaenlazada){
	PLista p=lista;
	int media=HallarMedia(lista);
	
	while(p!=NULL){
		
		if(p->dato <= media){
			
			
			InsertarPrincipioC(listacircular,p->dato);
		
		}else{
			InsertarPrincipioSE(listaenlazada,p->dato);
		}
		
		p=p->siguiente;
	}
}



int menu(){
    int opc;
       cout<<"Menu de listas doblemente enlazadas:"<<endl;
       cout<<"1. Crear lista "<<endl;
       cout<<"2. Distribuir"<<endl;
       cout<<"3. Mostrar lista circular de menores"<<endl;
       cout<<"4. Mostrar lista enlazada de mayores"<<endl;
       cout<<"0. Salir"<<endl;
       cout<< "Ingrese su opcion: ";
       cin >>opc;

    return  opc;

    }

 

	

int main()
{
	PLista lista =NULL;
	PListaC listacircular=NULL;
	PListaSE listaenlazada=NULL;
    int dato,pos;
    char rpt;
     int opc;
        do {
            opc = menu();
            switch(opc){
                case 1: do{
                        cout<<"ingrese el dato:";
                        cin>>dato;
                        InsertarFinal(lista,dato); 
						RecorrerLista(lista);
                        cout<<"Desea continuar?S/N:";
                        cin>>rpt;
                        }while(rpt=='s' || rpt=='S');

                          break;
                case 2:
                	    
				        
				        Distribuir(lista,listacircular,listaenlazada);
				        cout<<"La media es: "<< HallarMedia(lista)<<endl;
                        break;

		    case 3:     cout<<"lista circular con los menores o igual a la media:"<<endl;
						RecorrerListaC(listacircular);
						break;
						
			case 4:     cout<<"lista enlazada simple con los mayor a la media:"<<endl;
			            RecorrerListaSE(listaenlazada);
			            break;
               default:;

            }
            system("pause");  system("cls");
        }while(opc!=0);

	

   system ("pause");
   return(0);

}

