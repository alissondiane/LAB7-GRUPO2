/*
 * Escuela:
 * Curso:
 * Profesor:
 * alumno:
 * Fecha:
 * Descripcion:
 * 
 * 
 */
#include <iostream>
#include <stdio.h>
#include <stdlib.h>




using namespace std;
int cont=0;


struct Nodo{
	char dato;
	struct Nodo* siguiente;
	struct Nodo* anterior;
};

typedef struct Nodo* PLista;


PLista CrearNodo(char dato){
	PLista p = new (struct Nodo);
	p->dato=dato;
	p->siguiente= NULL;
	p->anterior= NULL;
	return p;
	
}

void RecorrerLista(PLista &lista){
	
	PLista p = lista;
	while(p != NULL){
		cout<<p->dato<<"->";
		p = p->siguiente;
	
	}
	cout<<"null"<<endl;
	
}

void InsertarPrincipio(PLista &lista,char dato){
	PLista nuevo =CrearNodo(dato);
	
	if(lista == NULL){
		
		lista=nuevo;
		
	}else{
		nuevo->siguiente = lista;
	    nuevo->anterior = NULL;
		lista->anterior=nuevo;
		lista=nuevo;
	}
	cont=cont+1;
}
void InsertarFinal(PLista &lista,char dato){
	PLista nuevo=CrearNodo(dato);
	if(lista == NULL){
		lista = nuevo;
	}else{
		PLista aux=lista;
		
		while(aux->siguiente!=NULL){
			aux=aux->siguiente;	
		}
		aux->siguiente=nuevo;
		nuevo->anterior=aux;
		nuevo->siguiente=NULL;
		
	}
	cont=cont+1;
}	
void InsertarPorPosicion(PLista &lista,char dato,int pos){
	PLista nuevo=CrearNodo(dato);
		if(pos<=cont && pos>0){
			if(pos==1){
				InsertarPrincipio(lista,dato);
			}else{
				int n=1;
				PLista aux=lista;
				while(n!=pos){
					aux=aux->siguiente;
					n++;
				}
				nuevo->siguiente=aux;
				nuevo->anterior=aux->anterior;
				aux->anterior=nuevo;
				nuevo->anterior->siguiente=nuevo;
				cont=cont+1;
			}
		}else{
			cout<<"posicion invalida"<<endl;
		}
	
}
void EliminarPrincipio(PLista &lista ){
    PLista aux=lista;
	if(aux!=NULL){
		PLista temporal=lista;
		if(aux->siguiente==NULL){
			lista = NULL;
		}else{
			lista = aux->siguiente;
			lista->anterior = NULL; 
		}
		delete temporal;
		cont=cont-1;
	}
         	
}
void EliminarFinal(PLista &lista){
    if(lista!=NULL){
		PLista aux=lista,temporal=NULL;
		if(aux->siguiente == NULL){
			aux->anterior=NULL;
			lista = NULL;
			temporal=aux;
		}else{
			while(aux->siguiente!=NULL){
			    aux = aux->siguiente;
		    }
		    aux->anterior->siguiente=NULL;
		    aux->anterior=NULL;
		    temporal=aux;
		}
		delete temporal;
	    cont=cont-1;
		
	}
}
void EliminarPorPosicion(PLista &lista,int pos){
	PLista p=lista;
	int n=1;
	if(lista!=NULL){
		if(pos<=cont && pos>0){
			if(pos==1){
				EliminarPrincipio(lista);
			}else{
				while(n!=pos){
		        p = p->siguiente;
		        n++;
	            }
	            if( p->siguiente!= NULL){
		            p->anterior->siguiente =p->siguiente;
		            p->siguiente->anterior = p->anterior;
		            delete p;
	            }else{
		            p->anterior->siguiente = NULL;
		            delete p;
	            }
				cont--;
			}
		}else{
			cout<<"posicion invalida:"<<endl;
			
		}
	}else{
		cout<<"lista vacia"<<endl;
	}
	
}



int menu(){
    int opc;
       cout<<"Menu de listas doblemente enlazadas:"<<endl;
       cout<<"------------------------------------"<<endl;
       cout<<"1. Ingresar los datos: "<<endl;
       cout<<"2. Convertir y mostrar el nuevo orden: "<<endl;
       cout<<"0. Salir"<<endl;
       cout<< "Ingrese su opcion: ";
       cin >>opc;

    return  opc;

    }



void Convertir(PLista &lista){
    EliminarPrincipio(lista);
    InsertarPorPosicion(lista,'R',3);
	
}

void Convertir2(PLista &lista,PLista &nuevalista){
	PLista p = lista;
	while(p != NULL){
	    InsertarPrincipio(nuevalista,p->dato);
		p = p->siguiente;
		
	
	}

	
}


	

int main()
{
	PLista lista =NULL;
	PLista listanueva=NULL;
    char dato,pos;
    char rpt;
     int opc;
        do {
            opc = menu();
            switch(opc){
                case 1: do{
                        cout<<"ingrese el dato:";
                        cin>>dato;
                        InsertarFinal(lista,dato); 
						RecorrerLista(lista);
                        cout<<"Desea continuar?S/N:";
                        cin>>rpt;
                        }while(rpt=='s' || rpt=='S');

                          break;
                case 2:
                	    
				         
                       Convertir(lista);
                       Convertir2(lista,listanueva);
                       RecorrerLista(listanueva);
                        
                        break;

		        default:;

            }
            system("pause");  system("cls");
        }while(opc!=0);

	

   system ("pause");
   return(0);

}

