
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;
int cont=0;
struct Nodo{
	int dato;
	struct Nodo* siguiente;
};
typedef struct Nodo* PLista;
PLista CrearNodo(int dato){
	PLista p = new (struct Nodo);
	p->dato=dato;
	p->siguiente=NULL;
	return p;
}
void RecorrerLista(PLista &lista){
	PLista p = lista;
	while(p != NULL){
		cout<<p->dato<<"->";
		p = p->siguiente;
	}
	cout<<"null"<<endl;
}
void InsertarPrincipio(PLista &lista,int dato){
	PLista nuevo=CrearNodo(dato);
	if(lista==NULL){
		lista=nuevo;
	}else{
	    nuevo->siguiente=lista;
	    lista=nuevo;
	}
	cont=cont+1;
}
void InsertarFinal(PLista &lista,int dato){
	PLista nuevo = CrearNodo(dato);
	if(lista == NULL){
		lista = nuevo;
	}else{
		PLista ultimo=lista;
		while(ultimo->siguiente != NULL){
			ultimo = ultimo->siguiente;
		}
		ultimo->siguiente = nuevo;
	}
    cont=cont+1;
}
void InsertarPosicion(PLista &lista,int dato,int pos){
	PLista nuevo = CrearNodo(dato);
	if(lista == NULL){
		cout<<"lista vacia";
	}else{
		if(pos<=cont && pos>0){
			if(pos==1){
			InsertarPrincipio(lista,dato);
		    }else{
		    int n=1;
		    PLista ant,act = lista;
		    while(n != pos){
			ant=act;
			act=act->siguiente;
			n++;
		    }
		    nuevo->siguiente=act;
		    ant->siguiente = nuevo;
		    cont=cont+1;
		    }
		}
		else{
			cout<<"posicion invalida"<<endl;
		}
	}
}
void EliminarPrincipio(PLista &lista ){
	if(lista==NULL){
		cout<<"lista vacia ";
	}else{
		PLista temporal = lista;
        lista = lista->siguiente;
        delete temporal;
        cont=cont-1;
	}
}
void EliminarFinal(PLista &lista){
	if(lista==NULL){
		cout<<"lista vacia";
	}else{
		if(lista->siguiente == NULL){
			delete lista;
			lista = NULL;
		}
		else{
			PLista ultimo =lista,ant,temporal;
		    while(ultimo->siguiente!=NULL){
			ant = ultimo;
			ultimo=ultimo->siguiente;
		    }
		    temporal = ant->siguiente;
	        ant->siguiente=NULL;
	        delete temporal;
		}
		cont=cont-1;
	   	}
}
void EliminarPosicion(PLista &lista,int pos){
	if(lista!=NULL){
		if(pos<=cont && pos>0){
			if(pos == 1){
				EliminarPrincipio(lista);
			}else{
				PLista act = lista,ant,temporal;
			    int n=1;
			    while(n!=pos){
				ant=act;
				act = act->siguiente;
				n=n+1;
			    }
			    temporal =ant->siguiente;
		 	    ant->siguiente=act->siguiente;
			    delete temporal;
			    cont--;
			}
		}
		else{
			cout<<"posicion invalida:"<<endl;
		}
	}else{
		cout<<"lista vacia"<<endl;
	}
}
void Eliminarpordato(PLista &lista){
PLista aux=lista;
int pos=1;
for(aux =lista; aux!=NULL; aux = aux->siguiente){
	if(aux->dato%2 == 0){
	  EliminarPosicion(aux,pos);
	}
	pos++;
}
}
void EliminarPares(PLista &lista){
	PLista p=lista,aux=lista;
	int pos = 1;
	while(p!=NULL){
		if((p->dato )% 2 == 0){
			EliminarPosicion(lista,pos);
		}
		pos++;
		p = p->siguiente;
	}
}
void intercambio(PLista &q, PLista &p){
    int aux;
    aux = p->dato;
    p->dato = q->dato;
    q->dato = aux;
}
PLista saltos (PLista q, int n){
    while(n!=0){
        q = q->siguiente;
       n = n-1;
    }
    return q;
}
void shell(PLista &lista, int cont){
    int numSal, paso=0;
    PLista q,p;
    p =lista;
    numSal = cont;
    bool e=false;
    do{
            e=false;
        numSal = int(numSal/2);
         cout<<"iteracion:  "<<endl;

        while(q!= NULL){
            q = saltos(p,numSal);
            if(q!=NULL){
                if(p->dato > q-> dato){
                    int aux;
                    aux = p->dato;
                    p->dato = q->dato;
                    q->dato = aux;
                    e=true;

                }
                paso++;
                p = p->siguiente;

                cout<<" paso "<<paso<<" : ";
                RecorrerLista(lista);
                cout<<endl;

            }
        }
        if(numSal>0 && e){
            shell(lista,2);
        }else if(e){
            shell(lista,2);
        }
    } while(numSal >1 );
}
int menu()
    {
      int opc;
       cout<<"Menu     "<<endl;
       cout<<"1. Adicionar al inicio"<<endl;
       cout<<"2. Adicionar al final"<<endl;
       cout<<"3. Adicionar por posicion"<<endl;
       cout<<"4. Eliminar primero"<<endl;
       cout<<"5. Eliminar ultimo"<<endl;
       cout<<"6. Eliminar poscion"<<endl;
       cout<<"7. Mostrar"<<endl;
       cout<<"8. Ordenamiento Shell"<<endl;
       cout<<"0. Salir"<<endl;
       cout<< "Ingrese su opcion: ";
       cin >>opc;
      return  opc;
    }
int main()
{
    PLista lista;
    lista = NULL;
    int dato,pos;
    char rpt;
     int opc;
        do {
            opc = menu();
            switch(opc){
                case 1: do{
                        cout<<"ingrese el dato:";
                        cin>>dato;
                        InsertarFinal(lista,dato);
						RecorrerLista(lista);
                        cout<<"Desea continuar?S/N:";
                        cin>>rpt;
                        }while(rpt=='s' || rpt=='S');
                          break;
                case 2:
				do{
                        cout<<"ingrese el dato:";
                        cin>>dato;
                        InsertarFinal(lista,dato);
						RecorrerLista(lista);
                        cout<<"Desea continuar?S/N:";
                        cin>>rpt;
                        }while(rpt=='s' || rpt=='S');
                        break;
                case 3:
                    do{
                        cout<<"ingrese el dato:";
                        cin>>dato;
                        cout<<"Ingrese posicion:";
                        cin>>pos;
                        InsertarPosicion(lista,dato,pos);
                        RecorrerLista(lista);
                        cout<<"Desea continuar?S/N";
                        cin>>rpt;
                        }while(rpt=='s' || rpt=='S');break;
               case 4:
                    do{
                        EliminarPrincipio(lista);
                        RecorrerLista(lista);
                            cout<<"Desea continuar?S/N";
                            cin>>rpt;
                        }while(rpt=='s' || rpt=='S');
						break;
               case 5:
                    do{
                        EliminarFinal(lista);
                        RecorrerLista(lista);
                        cout<<"Desea continuar?S/N";
                          cin>>rpt;
                        }while(rpt=='s' || rpt=='S');break;
            case 6:
                    do{ cout<<"Ingrese la posicion:";
                        cin>>pos;
                        EliminarPosicion(lista,pos);
                        RecorrerLista(lista);
                        cout<<"Desea continuar?S/N";
                        cin>>rpt;
                        }while(rpt=='s' || rpt=='S');
						break;
            case 7:
                        Eliminarpordato(lista);
                        RecorrerLista(lista);
						break;
             case 8:
                        shell(lista,cont);
                        RecorrerLista(lista);
						break;
               default:;
            }
        }while(opc!=0);
    system("pause");
   return(0);
    }
