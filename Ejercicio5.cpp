/*
 * Escuela:
 * Curso:
 * Profesor:
 * alumno:
 * Fecha:
 * Descripcion:
 *
 *
 */
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;
int cont=0;


struct Nodo{
	char dato;
	struct Nodo* siguiente;
	struct Nodo* anterior;
};

typedef struct Nodo* PLista;


PLista CrearNodo(char dato){
	PLista p = new (struct Nodo);
	p->dato=dato;
	p->siguiente= NULL;
	p->anterior= NULL;
	return p;

}

void RecorrerIda(PLista &lista){

	PLista p = lista;
	while(p != NULL){
		cout<<p->dato<<"->";
		p = p->siguiente;

	}
	cout<<"null"<<endl;

}

void RecorrerVuelta(PLista &lista){

	PLista p = lista;
	while(p->siguiente != NULL){
        p = p->siguiente;
	}

	while(p != NULL){
		cout<<p->dato<<"->";
		p = p->anterior;

	}
	cout<<"null"<<endl;

}

void InsertarPrincipio(PLista &lista,char dato){
	PLista nuevo =CrearNodo(dato);

	if(lista == NULL){

		lista=nuevo;

	}else{
		nuevo->siguiente = lista;
	    nuevo->anterior = NULL;
		lista->anterior=nuevo;
		lista=nuevo;
	}
	cont=cont+1;
}
void InsertarFinal(PLista &lista,char dato){
	PLista nuevo=CrearNodo(dato);
	if(lista == NULL){
		lista = nuevo;
	}else{
		PLista aux=lista;

		while(aux->siguiente!=NULL){
			aux=aux->siguiente;
		}
		aux->siguiente=nuevo;
		nuevo->anterior=aux;
		nuevo->siguiente=NULL;

	}
	cont=cont+1;
}
void InsertarPorPosicion(PLista &lista,char dato,int pos){
	PLista nuevo=CrearNodo(dato);
	if(lista == NULL){
		cout<<"La lista esta vacio no se puede agregar"<<endl;
	}else{
		if(pos<=cont && pos>0){
			if(pos==1){
				InsertarPrincipio(lista,dato);
			}else{
				int n=1;
				PLista aux=lista;
				while(n!=pos){
					aux=aux->siguiente;
					n++;
				}
				nuevo->siguiente=aux;
				nuevo->anterior=aux->anterior;
				aux->anterior=nuevo;
				nuevo->anterior->siguiente=nuevo;
				cont=cont+1;
			}
		}else{
			cout<<"posicion invalida"<<endl;
		}
	}
}
void EliminarPrincipio(PLista &lista ){
    PLista aux=lista;
	if(aux!=NULL){
		PLista temporal=lista;
		if(aux->siguiente==NULL){
			lista = NULL;
		}else{
			lista = aux->siguiente;
			lista->anterior = NULL;
		}
		delete temporal;
		cont=cont-1;
	}

}
void EliminarFinal(PLista &lista){
    if(lista!=NULL){
		PLista aux=lista,temporal=NULL;
		if(aux->siguiente == NULL){
			aux->anterior=NULL;
			lista = NULL;
			temporal=aux;
		}else{
			while(aux->siguiente!=NULL){
			    aux = aux->siguiente;
		    }
		    aux->anterior->siguiente=NULL;
		    aux->anterior=NULL;
		    temporal=aux;
		}
		delete temporal;
	    cont=cont-1;

	}
}


void InsertarMedio(PLista &lista, char antes, char despues, char dato){
    PLista p = lista;
    int i=2;
    while(p->siguiente != NULL){
        if(p->dato == antes && p->siguiente->dato == despues){
            InsertarPorPosicion(lista,dato,i);
        }
        p = p->siguiente;
        i++;
    }
}


int menu(){
    int opc;
       cout<<"Menu de listas doblemente enlazadas:"<<endl;
       cout<<"1. Llenar lista"<<endl;
       cout<<"2. Adicionar al inicio"<<endl;
       cout<<"3. Adicionar entre elementos"<<endl;
       cout<<"4. Mostrar"<<endl;
       cout<<"0. Salir"<<endl;
       cout<< "Ingrese su opcion: ";
       cin >>opc;
    return  opc;

    }





int main()
{
	PLista lista =NULL;
    int  pos;
    char rpt;
    char antes, despues, dato;
     int opc;
        do {
            opc = menu();
            switch(opc){
                case 1: do{
                        cout<<"ingrese el dato:";
                        cin>>dato;
                        InsertarFinal(lista,dato);
						RecorrerIda(lista);
                        cout<<"Desea continuar?S/N:";
                        cin>>rpt;
                        }while(rpt=='s' || rpt=='S');

                          break;
                case 2:
				do{
                        cout<<"ingrese el dato:";
                        cin>>dato;
                        InsertarPrincipio(lista,dato);
						RecorrerIda(lista);
                        cout<<"Desea continuar?S/N:";
                        cin>>rpt;
                        }while(rpt=='s' || rpt=='S');
                        break;
                case 3: cout<<"Ingrese el dato que desee ingresar"<<endl;
                        cin>>dato;
                        cout<<"�despues de que posicion desea ingresar"<<endl;
                        cin>>antes;
                        cout<<"�antes de que posicion desea ingresar"<<endl;
                        cin>>despues;
                        InsertarMedio(lista, antes, despues, dato);

                        ;break;

		    case 4:
                        cout<<"Recorrido de ida"<<endl;
                        RecorrerIda(lista);
                        cout<<"Recorrido de vuelta"<<endl;
                        RecorrerVuelta(lista);
						break;
               default:;

            }
              system("pause");  system("cls");
        }while(opc!=0);



   system ("pause");
   return(0);

}
